import 'dart:io';

import 'package:activity_record/model/diy_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:image_picker_saver/image_picker_saver.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:fluwx/fluwx.dart' as fluwx;

/*
图片查看器
包含图片和图片数量指示器
*/

class PhotoViewGalleryShow extends StatefulWidget {
  final List<DiyImage> images;
  final int index;
  final PageController pageController;
  PhotoViewGalleryShow({this.images, this.index})
      : pageController = new PageController(initialPage: index);

  @override
  State<StatefulWidget> createState() => _PhotoViewGalleryShowState();
}

class _PhotoViewGalleryShowState extends State<PhotoViewGalleryShow> {
  int _currentIndex;
  List<PhotoViewGalleryPageOptions> _list = [];

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _currentIndex = widget.index;
    _list = List.generate(widget.images.length, (index) {
      return new PhotoViewGalleryPageOptions(
        imageProvider:
            new AssetImage(widget.images[index].imageUrl,),
        heroTag: '$index',
        minScale: PhotoViewComputedScale.contained * 1.0,
        maxScale: PhotoViewComputedScale.covered * 2.0,
      );
    });
  }

  _showBottomSheet(BuildContext context int currentIndex) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return new Container(
            height: 195.0,
            child: new Column(
              children: <Widget>[
              new ListTile(
                leading: new Icon(Icons.share),
                title: new Text('发送微信好友',style: Theme.of(context).textTheme.button.copyWith(fontSize: 18.0),),
                onTap: () {
                  Navigator.of(context).pop();
                  _shareImage(currentIndex);
                },
              ),
                new Container(
                  color: Theme.of(context).dividerColor,
                  height: 1.0,
                ),
                new ListTile(
                  leading: new Icon(Icons.file_download),
                  title: new Text('保存图片',style: Theme.of(context).textTheme.button.copyWith(fontSize: 18.0),),
                  onTap: () {
                    Navigator.of(context).pop();
                    _imageSave(currentIndex);
                  },
                ),
                new Container(
                  color: Theme.of(context).dividerColor,
                  height: 4.0,
                ),
                new ListTile(
                  leading: new Icon(Icons.cancel),
                  title: new Text('取消',style: Theme.of(context).textTheme.button.copyWith(fontSize: 18.0),),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                new SizedBox(
                  height: 8.0,
                  ),
              ],
            ),
          );
        },);
  }

  _imageSave(int currentIndex) async {
    await ImagePickerSaver.saveFile(
            fileData: new File(widget.images[currentIndex].imageUrl).readAsBytesSync())
        .whenComplete(() {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        duration: new Duration(milliseconds: 1000),
        content: new Text('图片已保存'),
      ));
    });
  }

  _shareImage(int currentIndex){
    String _imagePath = '${widget.images[currentIndex].imageUrl}';
    print('微信分享图片地址是：assets:${_imagePath}');
    fluwx.share(fluwx.WeChatShareImageModel(
      image: 'assets://var/mobile/Media/DCIM/100APPLE/IMG_0551.JPG',
      scene: fluwx.WeChatScene.SESSION,
      description: 'image'));
  }

  

  _pageChanged(index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: new GestureDetector(
          onLongPress: () {
            _showBottomSheet(context,_currentIndex);
          },
          onTap: () => Navigator.of(context).pop(),
          child: new Container(
            child: new Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                new PhotoViewGallery(
                  pageOptions: _list,
                  pageController: widget.pageController,
                  onPageChanged: (int) {
                    _pageChanged(int);
                  },
                ),
                new Padding(
                  padding: const EdgeInsets.only(bottom: 18.0),
                  child: new Text(
                    '${_currentIndex + 1}/${widget.images.length}',
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
          ),
        ),);
  }
}
