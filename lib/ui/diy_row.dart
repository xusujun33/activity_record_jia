import 'package:activity_record/model/diy.dart';
import 'package:activity_record/my_flutter_app_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:transparent_image/transparent_image.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:transparent_image/transparent_image.dart';

/*
 首页列表中diy信息ui布局
*/

class DiyRow extends StatelessWidget {
  final Diy diy;
  final int index;
  final ValueChanged<int> valueChanged;

  DiyRow({this.diy, this.index, this.valueChanged});

  //点击右上角按钮操作
  _handleTaped() {
    valueChanged(index);
  }

  //图标和文本组合row
  Row iconAndText(IconData iconData, String text,
      {double iconSize: 18.0, double fontSize: 14.0, Color color}) {
    return new Row(
      children: <Widget>[
        new Icon(
          iconData,
          size: iconSize,
          color: color,
        ),
        new SizedBox(
          width: 8.0,
        ),
        new Expanded(
          child: new Text(
            text,
            style: new TextStyle(
              fontSize: fontSize,
              color: color,
            ),
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    //card第三行时间地点
    Widget _rowTime(Diy diy, int indexSelected) {
      return new Container(
        //设置距离左边8.0的内间距
        padding: const EdgeInsets.fromLTRB(15.0, 8.0, 8.0, 4.0),
        child: new Row(
          children: <Widget>[
            new Expanded(
                child: iconAndText(Xusj.time, diy.diyProject.date,
                    iconSize: 16.0)),
            new Expanded(
              child: iconAndText(Xusj.local1, diy.diyProject.place),
            ),
          ],
        ),
      );
    }

    //是否结款小标签
    Widget buildContainer(String text, int color) {
      return new Container(
        padding: const EdgeInsets.all(4.0),
        child: new Text(
          text,
          style: new TextStyle(
              fontSize: 10.0, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        decoration: new BoxDecoration(
            color: Color(color), borderRadius: new BorderRadius.circular(4.0)),
      );
    }

    //card第二行名称和结款状态
    Widget _rowNameAndPlace(Diy diy) {
      return new Container(
        padding: const EdgeInsets.only(left: 15.0, top: 8.0, right: 15.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Text(
              diy.diyProject.name,
              style: new TextStyle(fontSize: 19.0, fontWeight: FontWeight.w600),
            ),
            diy.diyProject.isPaided == true
                ? buildContainer('已结款', 0xffbf5a40)
                : buildContainer('未结款', 0xffe3ac2d),
          ],
        ),
      );
    }

    //进行card里的内容组合
    Widget _diyContentShow(BuildContext context, Diy diy, int index) {
      return Container(
        height: 288.0,
        child: new Stack(
          alignment: Alignment.bottomRight,
          children: <Widget>[
            new Column(
              children: <Widget>[
                //使用expanded将填充控件的剩余空间
                new Expanded(
                  //flex代表这个控件在父控件里的范围比例，默认是1，这里表示在高度288的容器里，图片会填满剩余的所有空间
                  flex: 3,
                  child: new ClipRRect(
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(4.0)),
                      child: FadeInImage(
                        placeholder: new MemoryImage(kTransparentImage),
                        image: new AssetImage(diy.imageDatas.first.imageUrl),
                        fit: BoxFit.cover,
                        width: MediaQuery.of(context).size.width,
                      )),
                ),
                _rowNameAndPlace(diy),
                _rowTime(diy, index),
                //单价份数和菜单按钮
                new Padding(
                  padding: const EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 8.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          new Text(
                            '${diy.diyProject.singlePrcie.toString()}元',
                            style: new TextStyle(
                                fontSize: 17.0,
                                color: Color(0xFFda7660),
                                fontWeight: FontWeight.bold),
                          ),
                          new SizedBox(
                            width: 50.0,
                          ),
                          new Text(
                            '${diy.diyProject.nums.toString()}份',
                            style: new TextStyle(
                                fontSize: 17.0,
                                color: Color(0xFFda7660),
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            new IconButton(
              icon: new Icon(Icons.more_horiz),
              onPressed: _handleTaped,
            )
          ],
        ),
      );
    }

    return new Card(
      elevation: 0.4,
      margin: const EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 6.0), //设置外边距18
      //card形状设置顶部圆形弧度12，底部没有
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      child: _diyContentShow(context, diy, index),
    );
  }
}
