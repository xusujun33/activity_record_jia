/*
图片选择器选后图片显示
包含图片和右上角删除按钮
*/

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';

class ImagePicked extends StatelessWidget {
  final String imageUrl;
  final int index;
  final ValueChanged<int> valueChanged;
  final String path;
  ImagePicked({this.imageUrl, this.index, this.valueChanged,this.path});

  void _handleCancel() {
    valueChanged(index);
  }

  @override
  Widget build(BuildContext context) {
    return new Stack(
      alignment: Alignment(0.92, -0.92),
      children: <Widget>[
        new FadeInImage(
          placeholder: new MemoryImage(kTransparentImage),
          image: new AssetImage('${path}${imageUrl}'),
          fit: BoxFit.cover,
          width: (MediaQuery.of(context).size.width - 32.0 - 8.0) / 3,
          height: (MediaQuery.of(context).size.width - 32.0 - 8.0) / 3,
        ),
        new GestureDetector(
          onTap: _handleCancel,
          child: new Container(
            decoration: new BoxDecoration(
              color: Colors.black45,
              shape: BoxShape.circle,
            ),
            child: new Icon(
              Icons.close,
              color: Colors.white,
              size: 20.0,
            ),
          ),
        )
      ],
    );
  }
}
